# README #

### What is this repository for? ###
This project was originally built to be a proof of concept of Unity's networking systems. However, as I played around with it, I ended up making it into a parkour platforming framework. 

### How do I get set up? ###

This version of the game can be played with a mouse and keyboard or a controller

The game executable can be found  in the downloads section of this repository!