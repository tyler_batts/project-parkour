﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIScript : MonoBehaviour {

	[SerializeField] GameObject PauseMenuPopup, player;
	[SerializeField] Button resumeButton;
	bool paused;

	void Update () {
		GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
		GetComponent<Canvas>().worldCamera = Camera.main;
		GetComponent<Canvas>().planeDistance = 0.31f;

		if (MultiPlatformInput.GetButtonDown("Pause")) {
			if (!paused) {
				paused = true;
				Pause();
			} else {
				paused = false;
				Resume();
			}
		}
	}

	public void Pause() {
		PauseMenuPopup.SetActive(true);
		player.GetComponent<CharacterController>().enabled = false;
		player.GetComponent<MovementController>().enabled = false;
	}

	public void Resume() {
		PauseMenuPopup.SetActive(false);
		player.GetComponent<CharacterController>().enabled = true;
		player.GetComponent<MovementController>().enabled = true;
	}
}
