﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerSyncPosition : NetworkBehaviour {

	[SyncVar] private Vector3 syncPos;

	[SerializeField] Transform myTransform;
	[SerializeField] float lerpRate = 15f;

	private Vector3 lastPos;
	private float threshhold = 0.5f;

	void Update() {
		LerpPosition();
	}

	void FixedUpdate () {
		TransmitPosition();
	}

	void LerpPosition () {
		if (!isLocalPlayer) {
			myTransform.position = Vector3.Lerp(myTransform.position, syncPos, Time.deltaTime*lerpRate);
		}
	}

	[Command]
	void CmdProvidePositionToServer (Vector3 pos) {
		//Debug.Log("Command for position");
		syncPos = pos;
	}

	[ClientCallback]
	void TransmitPosition() {
		if (isLocalPlayer && Vector3.Distance(myTransform.position, lastPos) > threshhold) {
			CmdProvidePositionToServer(myTransform.position);
			lastPos = myTransform.position;
		}
	}
}
