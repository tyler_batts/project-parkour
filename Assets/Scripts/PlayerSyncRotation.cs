﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerSyncRotation : NetworkBehaviour {

	[SyncVar] private Quaternion syncPlayerRotation;
	[SyncVar] private Quaternion syncCamRotation;

	[SerializeField] private Transform playerTransform;
	[SerializeField] private Transform camTransform;
	[SerializeField] private float lerpRate = 15f;

	private Quaternion lastPlayerRot;
	private Quaternion lastCamRot;
	private float threshold = 5;

	void Update () {
		if (GetComponent<MovementController>().fpsCam.enabled) {
			camTransform = GetComponent<MovementController>().fpsCam.transform;
		} else {
			camTransform = GetComponent<MovementController>().tpsCam.transform;
		}
		LerpRotations();
	}

	void FixedUpdate () {
		TransmitRotations();
	}

	void LerpRotations() {
		if (!isLocalPlayer) {
			playerTransform.rotation = Quaternion.Lerp(playerTransform.rotation, syncPlayerRotation, lerpRate*Time.deltaTime);
			camTransform.rotation = Quaternion.Lerp(camTransform.rotation, syncCamRotation, lerpRate*Time.deltaTime);
		}
	}

	[Command]
	void CmdProvideRotationsToServer(Quaternion playerRotation, Quaternion camRotation) {
		//Debug.Log("Command for angle");
		syncPlayerRotation = playerRotation;
		syncCamRotation = camRotation;
	}

	[Client]
	void TransmitRotations () {
		if (isLocalPlayer) {
			if (Quaternion.Angle(playerTransform.rotation, lastPlayerRot) > threshold || Quaternion.Angle(camTransform.rotation, lastCamRot) > threshold) {
				CmdProvideRotationsToServer(playerTransform.rotation, camTransform.rotation);
				lastPlayerRot = playerTransform.rotation;
				lastCamRot = camTransform.rotation;
			}
		}
	}
}
