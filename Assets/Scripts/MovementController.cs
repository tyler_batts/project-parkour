﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

[RequireComponent(typeof (CharacterController))]
[RequireComponent(typeof (ParkourController))]
public class MovementController : MonoBehaviour {

	[SerializeField] private CameraLook cameraLook;
	[SerializeField] private PlayerStats playerStats = new PlayerStats();
	public Camera fpsCam, tpsCam;
	[SerializeField] private float stickToGroundForce, gravityMultiplier;

	private CharacterController characterController;
	private ParkourController parkour;
	private CollisionFlags collisionFlags;
	private Camera mainCamera;

	private Vector2 input;
	private Vector3 moveDir;
	private bool jump, isJumping, isRunning, isSprinting;
	private float speed;

	void OnEnable()
	{
		characterController = GetComponent<CharacterController>();
		parkour = GetComponent<ParkourController>();

		SetCamera();
		cameraLook.Init(transform, mainCamera.transform);
	}

	void Update() {
		SetCamera();
		RotateView();

		if (MultiPlatformInput.GetButtonDown("Jump") && !parkour.IsClimbing) {
			jump = true;
		}

		if (characterController.isGrounded) {
			isJumping = false;
		}

		if (MultiPlatformInput.GetButtonDown("PerspectiveSwitch")) {
			if (Camera.main.name == "FirstPersonCamera")
				SwitchCameraPerspective("tps");
			else if (Camera.main.name == "ThirdPersonCamera")
				SwitchCameraPerspective("fps");
		}

		#if DEVELOPMENT_BUILD || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Backspace)) {
			transform.position = GameObject.Find("Spawn").transform.position;
			transform.rotation = Quaternion.Euler(Vector3.zero);
			mainCamera.transform.rotation = Quaternion.Euler(Vector3.zero);
		}
		#endif
	}

	void FixedUpdate() {
		speed = 0;
		GetInput(out speed);

		// always move along the camera forward as it is the direction that it being aimed at
		Vector3 desiredMove = transform.forward*(MultiPlatformInput.GetAxis("Vertical")) + transform.right*(MultiPlatformInput.GetAxis("Horizontal"));

		// get a normal for the surface that is being touched to move along it
		RaycastHit hitInfo;
		Physics.SphereCast(transform.position, characterController.radius, Vector3.down, out hitInfo,
			characterController.height/2f, ~0, QueryTriggerInteraction.Ignore);
		desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

		moveDir.x = desiredMove.x*speed;
		moveDir.z = desiredMove.z*speed;

		if (characterController.isGrounded) {
			moveDir.y = -stickToGroundForce;
			if (jump) {
				Jump();
			}
		} else {
			moveDir += Physics.gravity*gravityMultiplier*Time.fixedDeltaTime;
		}
		parkour.RunParkour(speed);
		if (!parkour.IsClimbing && !parkour.IsHanging && !parkour.IsWallRunning)
			collisionFlags = characterController.Move(moveDir*Time.fixedDeltaTime);
		cameraLook.UpdateCursorLock();
	}

	public void Jump () {
		moveDir.y = playerStats.jumpSpeed;
		jump = false;
		isJumping = true;
	}

	private void GetInput(out float speed)
	{
		// Read input
		float horizontal = MultiPlatformInput.GetAxis("Horizontal");
		float vertical = MultiPlatformInput.GetAxis("Vertical");

		bool waswalking = isRunning;

		#if !MOBILE_INPUT
		// On standalone builds, walk/run speed is modified by a key press.
		// keep track of whether or not the character is walking or running
		isRunning = !MultiPlatformInput.GetButton("Sprint");
		if ((MultiPlatformInput.GetAxis("Sprint") > 0 || MultiPlatformInput.GetButton("Sprint")) && MultiPlatformInput.GetAxis("Vertical") > 0) {
			isRunning = false;
		}
		isSprinting = !isRunning;
		#endif
		// set the desired speed to be walking or running
		speed = isRunning ? playerStats.runSpeed : playerStats.sprintSpeed;
		input = new Vector2(horizontal, vertical);

		// normalize input if it exceeds 1 in combined length:
		if (input.sqrMagnitude > 1)
		{
			input.Normalize();
		}
	}

	private void RotateView()
	{
		cameraLook.LookRotation (transform, mainCamera.transform);
	}
		
	void SetCamera() {
		if (PlayerPrefs.GetString("CameraPerspective").ToLower().Equals("tps")) {
			fpsCam.enabled = false;
			fpsCam.GetComponent<AudioListener>().enabled = false;
			tpsCam.enabled = true;
			tpsCam.GetComponent<AudioListener>().enabled = true;
			mainCamera = tpsCam;
		} else if (PlayerPrefs.GetString("CameraPerspective").ToLower().Equals("fps")) {
			fpsCam.enabled = true;
			fpsCam.GetComponent<AudioListener>().enabled = true;
			tpsCam.enabled = false;
			tpsCam.GetComponent<AudioListener>().enabled = false;
			mainCamera = fpsCam;
		} else {
			PlayerPrefs.SetString("CameraPerspective", "fps");
			fpsCam.enabled = true;
			fpsCam.GetComponent<AudioListener>().enabled = true;
			tpsCam.enabled = false;
			tpsCam.GetComponent<AudioListener>().enabled = false;
			mainCamera = fpsCam;
		}
	}

	public void SwitchCameraPerspective(string toPerspective) {
		switch (toPerspective) {
		case "fps":
			PlayerPrefs.SetString("CameraPerspective", "fps");
			break;

		case "tps":
			PlayerPrefs.SetString("CameraPerspective", "tps");
			break;

		default:
			break;
		}
	}

	#region Properties
	public bool CursorIsLocked {
		get {
			return cameraLook.CursorIsLocked;
		}

		set {
			cameraLook.CursorIsLocked = value;
		}
	}

	public bool GetJump {
		get {
			return jump;
		}
	}

	public bool IsJumping {
		get {
			return isJumping;
		}
	}

	public float RunSpeed {
		get {
			return playerStats.runSpeed;
		}
	}

	public float GravityMultiplier {
		get {
			return gravityMultiplier;
		}
	}

	public Vector3 MoveDir {
		get {
			return moveDir;
		}
	}
	#endregion
}