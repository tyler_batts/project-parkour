﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	void Update() {
		#if DEVELOPMENT_BUILD
		if (Input.GetButtonDown("DebugQuit") && Input.GetButtonDown("PerspectiveSwitch")) {
			Application.Quit();
		}
		#endif
	}
}
