﻿using UnityEngine;
using System.Collections;

public static class MultiPlatformInput {

    static bool controllerConnected;
    static string buttonToChange = "Sprint";

	public static bool isControllerConnected {
		get {
			if (Input.GetJoystickNames().Length > 0) {
				return Input.GetJoystickNames()[0].ToString() != "";
			} else {
				return false;
			}
		}
	}

	public static bool GetButtonDown (string button) {
		if (Input.GetJoystickNames().Length > 0) {
			if (Input.GetJoystickNames()[0] == "") {
				return Input.GetButtonDown(button);
			} else {
				if (button == buttonToChange) {
					return Input.GetAxis("Joystick " + buttonToChange) > 0f;
				} else {
					return Input.GetButtonDown("Joystick " + button);
				}
			}
		} else {
			return Input.GetButtonDown(button);
		}
	}

	public static bool GetButton (string button) {
		if (Input.GetJoystickNames().Length > 0) {
			if (Input.GetJoystickNames()[0] == "") {
				return Input.GetButton(button);
			} else {
                if (button == buttonToChange) {
                    return Input.GetAxis ("Joystick "+ buttonToChange) > 0f;
                } else {
                    return Input.GetButton ("Joystick " + button);
                }
            }
		} else {
			return Input.GetButton(button);
		}
	}

	public static bool GetButtonUp (string button) {
		if (Input.GetJoystickNames().Length > 0) {
			if (Input.GetJoystickNames()[0] == "") {
				return Input.GetButtonUp(button);
			} else {
                if (button == buttonToChange) {
                    return Input.GetAxis ("Joystick " + buttonToChange) > 0f;
                } else {
                    return Input.GetButtonUp ("Joystick " + button);
                }
            }
		} else {
			return Input.GetButtonUp(button);
		}
	}

	public static float GetAxis (string axis) {
		if (Input.GetJoystickNames().Length > 0) {
			if (Input.GetJoystickNames()[0] == "") {
				return Input.GetAxis(axis);
			} else {
				return Input.GetAxis("Joystick " + axis);
			}
		} else {
			return Input.GetAxis(axis);
		}
	}
}
