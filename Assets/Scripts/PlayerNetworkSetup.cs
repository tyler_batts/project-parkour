﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerNetworkSetup : NetworkBehaviour {

	[SerializeField]
	Camera fpsCam, tpsCam;

	void Start () {
		if (isLocalPlayer) {
			if (PlayerPrefs.GetString("CameraPerspective").ToLower().Equals("fps")) {
				fpsCam.enabled = true;
				fpsCam.GetComponent<AudioListener>().enabled = true;
				tpsCam.enabled = false;
				tpsCam.GetComponent<AudioListener>().enabled = false;
			} else if (PlayerPrefs.GetString("CameraPerspective").ToLower().Equals("tps")) {
				fpsCam.enabled = false;
				fpsCam.GetComponent<AudioListener>().enabled = false;
				tpsCam.enabled = true;
				tpsCam.GetComponent<AudioListener>().enabled = true;
			}
				
			GameObject.Find("Scene Camera").SetActive(false);
			GetComponent<CharacterController>().enabled = true;
			GetComponent<MovementController>().enabled = true;
			GetComponent<ParkourController>().enabled = true;
		}
	}
}
