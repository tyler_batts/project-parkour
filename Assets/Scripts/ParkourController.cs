﻿using UnityEngine;
using System.Collections;

public class ParkourController : MonoBehaviour {

	private CharacterController characterController;
	private Camera mainCamera;
	private MovementController movement;
	private Vector3 climbDestinationPoint, direction;
	private Vector2 input;

	[SerializeField] LayerMask parkourableObjectsLayerMask;

	private bool canWallRun = true;
	private bool isWallRunning, canClimb, isClimbing, isHanging;

	[SerializeField] private float wallRunGravityMultiplier = 0.75f, wallrunRaycastLength = 1.0f;
	Vector3 wallRunDestinationPoint, desiredClimb;

	Vector3 tempVector3;

	void Start() {
		characterController = GetComponent<CharacterController>();
		movement = GetComponent<MovementController>();
		mainCamera = Camera.main;
	}

	public void RunParkour (float speed) {
		direction = movement.MoveDir;
		direction.y = 0;
		direction = direction.normalized;
		Ray temp = new Ray(transform.position, direction);

		#region Climb Up
		RaycastHit climbHitInfo;
		if (Physics.Raycast(temp, out climbHitInfo, 1f, parkourableObjectsLayerMask)) {
			isHanging = true;
			canClimb = true;
			climbDestinationPoint = climbHitInfo.point + new Vector3(0, climbHitInfo.collider.bounds.size.y, 0);
		}

		if (canClimb) {
			if (isHanging && !isClimbing) {
				HangOnLedge(climbHitInfo);
			} else {
				ClimbObject(climbHitInfo);
				if ((climbDestinationPoint - transform.position).magnitude < 0.25f)
					canClimb = false;
			}
		} else {
			isClimbing = false;
			isHanging = false;
		}

		if (characterController.isGrounded) {
			canClimb = false;
		}
		#endregion
	}

	private void HangOnLedge (RaycastHit obstacle) {
		if (!characterController.isGrounded) {
			if (MultiPlatformInput.GetButton("Jump")) {
				if (Physics.Raycast(new Ray(transform.position, direction), 1f, parkourableObjectsLayerMask)) {
					isHanging = false;
					isClimbing = true;
				}
			} else {
				transform.position = transform.position;
			}

			if (MultiPlatformInput.GetButtonDown("Jump")) {
				if (!Physics.Raycast(new Ray(transform.position, direction), 1f, parkourableObjectsLayerMask)) {
					canClimb = false;
					isHanging = false;
					isClimbing = false;
					movement.Jump();
					return;
				}
			}
		}
	}

	private void ClimbObject (RaycastHit obstacle) {
		if (!characterController.isGrounded && MultiPlatformInput.GetAxis("Vertical") > 0) {
			isClimbing = true;
			characterController.Move((climbDestinationPoint - transform.position)*0.16f);
		} else {
			isClimbing = false;
			canClimb = false;
		}
	}

	void OnTriggerEnter (Collider other) {
		canWallRun = true;
		if (other.tag == "Parkour") {
			desiredClimb = new Vector3(characterController.velocity.x, movement.RunSpeed/2, characterController.velocity.z);
		}
	}

	void OnTriggerStay(Collider other) {
		if (other.tag == "Parkour") {
			//Physics.Raycast(new Ray(transform.position, direction), wallrunRaycastLength) && 
			if ((MultiPlatformInput.GetButton("Sprint")) && MultiPlatformInput.GetAxis("Vertical") > 0 && canWallRun && !movement.IsJumping) {
				isWallRunning = true;
				if (movement.GetJump) {
					canWallRun = false;
					isClimbing = false;
					movement.Jump();
					return;
				}
				characterController.Move(desiredClimb*0.02f);
			} else {
				isWallRunning = false;
			}

			if (characterController.isGrounded) {
				desiredClimb = new Vector3(characterController.velocity.x, movement.RunSpeed/2, characterController.velocity.z);
				canWallRun = true;
			}

			if (isWallRunning) {
				desiredClimb += Physics.gravity*wallRunGravityMultiplier*Time.fixedDeltaTime;
			}
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.tag == "Parkour") {
			isWallRunning = false;
			canWallRun = true;
		}
	}

	#region Properties
	public bool CanClimb {
		get {
			return canClimb;
		}
	}

	public bool IsClimbing {
		get {
			return isClimbing;
		}
	}

	public bool IsHanging {
		get {
			return isHanging;
		}
	}

	public bool IsWallRunning {
		get {
			return isWallRunning;
		}
	}
	#endregion
}
